#!/usr/bin/env python

#UWAGA!
#ten program wymaga zmodyfikowanej wersji scapy- standardowa na linuxa zawiera buga
#http://comments.gmane.org/gmane.comp.security.scapy.general/4699
#w klasie RTP scapy trzeba zamienic nazwe pola opisujacego payload-type protokolu RTP
#z "payload" na "payload_type"
#na Windowsie nie zauwazono problemu

import logging
l=logging.getLogger("scapy.runtime")
l.setLevel(49)

import os,sys
from scapy.all import *
from scapy.layers.rtp import RTP
from scapy.layers.inet import UDP

try:
	# Win32
	from msvcrt import getch
except ImportError:
	# UNIX
	def getch():
		import sys, tty, termios
		fd = sys.stdin.fileno()
		old = termios.tcgetattr(fd)
		try:
			tty.setraw(fd)
			return sys.stdin.read(1)
		finally:
			termios.tcsetattr(fd, termios.TCSADRAIN, old)


src_sync = None
last_byte = 0
data_len = 0
finished = False 
data = None
fname = None
key = '0123456789abcdef'	#tym szyfrujemy, ale to moze byc proste, bo to tylko, zeby nei bylo widac tekstu w transmisji.
pcap_data = None



file_to_receive_size = 51700	#rozmiar pliku, ktory mamy zamiar odczytac, do sprawdzenia, czy wszystko ok
lack_mid_time = 140 # patrz send.py
min_lack_time = (lack_mid_time - 10)/1000.0 # ms na s
max_wait_time = 1 # s jak daleko od sprawdzanego pakietu jest szukany pakiet lack
time_to_start = 1 # po ilu sekundach tranismisji rtp ma zaczac szukac pliku

def read_data(data):
	print "Processing pcap file..."

	global min_lack_time, max_wait_time, time_to_start
	recieved_data = bytearray()
	stegano_sourcesync = None
	stegano_sourcesync_counter = 0
	first_rtp_time = -sys.maxint - 1 	# najmniejszy int w pythonie - zaraz sie przyda do przyrownywania - czas rozpoczecia transmisji rtp
	max_dif = 0

	i=0					#numer pakietu od ktorego bedzie szukany opozniony pakiet
	while i < len(data)-1 :
		print "processing {0} of {1}".format(i, len(data))
		predecessor = data[i]		# pakiet od ktorego bedzie szukany opozniony pakiet
		correct_predecessor = False 	# flaga czy ten pakiet spelnia zalozenia - pakiet RAW z warstwa Raw
		lack_found = False 		# flaga czy znaleziono pakiet celowo opozniony metoda lack w petli z j (nizej)
		if  predecessor.time - first_rtp_time >= time_to_start : # sprawdzanie, czy od poczatku transmisji rtp minal umowny czas, po ktorym moze rozpoczac sie steganografia	
			if predecessor.haslayer(Raw) and predecessor.haslayer(UDP):
				try :
					predecessor.getlayer(UDP).decode_payload_as(RTP)
				except :
					i+=1
					continue
				if predecessor.haslayer(Raw):	# sprawdzenie, czy odpowiedni pakiet rpt
					if first_rtp_time == -sys.maxint - 1  :		#
						print "first RTP found"			#
						first_rtp_time = predecessor.time 	# jesli nie ustalono jeszcze czasu rozpoczecia transmisji rtp to ustaw ten czas
						i+=1					# i szukamy dalej, az znajdziemy pakiet, w ktorym minal zadany czas od rozpoczecia transmisji
						continue				# do rozpoczecia steganografii
					if stegano_sourcesync == None or predecessor[RTP].sourcesync == stegano_sourcesync :	#jesli jeszce nie odkryro sourcesynca transmisji w ktorej jest
 													#steganografia, albo jesli dany pakiet ma sourcesync taki jak transmisja, 
													#w ktorej jest steganografia (nalezy do tej transmisji), ustaw ten pakiet jako poprawny 
													#pakiet odniesienia
						correct_predecessor = True
						predecessor_seq = predecessor[RTP].sequence
						predecessor_time = predecessor.time
						predecessor_sync = predecessor[RTP].sourcesync

		if first_rtp_time == None :
			#print "first RTP packet not found yet"
			i+=1
			continue
		
		if not predecessor.time - first_rtp_time >= time_to_start :
			#print "time to start not passed yet"
			i+=1
			continue

		if not correct_predecessor :
			#print "not correct predecessor"
			i+=1
			continue

		successor_time = predecessor_time #inicjalizacja czasu szukanego pakietu lack czasem pakietu odniesienia
		j = i+1
		successor = data[j]
		
		while successor.time - predecessor_time <= max_wait_time : # szuka pakietu w oknie okreslonym przez max_wait_time
			if successor.haslayer(Raw) and successor.haslayer(UDP):
				try :
					successor.getlayer(UDP).decode_payload_as(RTP)
				except :
					j+=1
					if j >=len(data):
						break
					successor = data[j]
					continue
				if successor.haslayer(Raw) :		# sprawdzenie, czy odpowiedni pakiet rtp
#					if len(successor[Raw].load) >=900 and len(successor[Raw].load) <= 1100 :
					if len(successor[Raw].load) <= 1100 : # sprawdzenie, czy miesci sie w ramy okreslone przez nasza steganografie
						if successor[RTP].sourcesync == predecessor_sync :	# czy ma takie samo sourcesync jak poprzednik
							if successor[RTP].sequence == predecessor_seq + 1 :	# czy jest kolejnym pakietem w sekwencji poprzednika
								#print " {} - {} = {} ? >= {} ".format(successor.time, predecessor_time, successor.time - predecessor_time, min_lack_time )
								if successor.time - predecessor_time > max_dif : #tutaj licze w ramach badan maksymalne opoznienie miedzy pakietami
									max_dif = successor.time - predecessor_time
								if successor.time - predecessor_time >= min_lack_time : #jesli roznica w czasie miedzy pakietami wieksza rowna niz czas o ktory 
									print "LACK delayed packet found"		#opozniamy, to jest to zapewne LACK pakeit
									lack_found = True
									#print "predecessor: "
									#predecessor.show()
									#print "successor: "
									#successor.show()
									#lack_found = True
									if stegano_sourcesync == None :			# jesli nie ustalono sourcesynca transmisji w ktorej jest steganografia to ustaw
										stegano_sourcesync = successor[RTP].sourcesync	
									recieved_data+=bytes(successor[Raw].load)	#pobranie danych z pakietu
									break
			j+=1
			if j >=len(data):
				break
			successor = data[j]

		if lack_found :			
			i = j+1	
		else:
			i+=1
	#print "max diff: {}".format(max_dif)
	return recieved_data


def read_pcap_file(name):
	print "Reading Pcap file..."
	pcap = rdpcap(name)
	return pcap

def get_filename():
	global fname, pcap_data
	fname = raw_input("Please enter pcap file to retreive steganographic data: ")
	try:
		pcap_data = read_pcap_file(fname)
	except:
		print "Can't open file. Try again"
		get_filename()


def write_file(name, recieved_data):
	print "Saving received file..."
	f = open(name, 'wb')
	f.write(bytes(recieved_data))
	f.close()

def decrypt_file(key, in_filename, out_filename=None, chunksize=24*1024):
	if not out_filename:
		out_filename = os.path.splitext(in_filename)[0]

	with open(in_filename, 'rb') as infile:
		origsize = struct.unpack('<Q', infile.read(struct.calcsize('Q')))[0]
		iv = infile.read(16)
		decryptor = AES.new(key, AES.MODE_CBC, iv)

		with open(out_filename, 'wb') as outfile:
			while True:
				chunk = infile.read(chunksize)
				if len(chunk) == 0:
					break
				outfile.write(decryptor.decrypt(chunk))

			try:
            			outfile.truncate(origsize)
	   		except OverflowError: 
				print "There may be some spelling errors in received file due to transmission errors, but file is ok"
	
def main():
	global fname, pcap_data, file_to_receive_size
	print "projekt OINS reader by Mateusz Koslacz and Pawel Jagiello."
	print ""
	print "WARNING!!! Scapy for linux has a bug that preclude proper execution of this program. Check header comment of this file to find out how to fix it."
	print ""

	try:
		get_filename()
		rec_data = read_data(pcap_data)
	except KeyboardInterrupt:
		print ""
		print "Interrupted by user, exiting..."
		sys.exit(1)

	if len(rec_data) >= file_to_receive_size:
		print "This file contains Antygona.txt - saved into {0}.txt".format(fname)
		write_file('{0}.txt.enc'.format(fname), rec_data)
		decrypt_file(key, '{0}.txt.enc'.format(fname))
		os.remove('{0}.txt.enc'.format(fname))
	else:
		print "This file does not contains Antygona.txt"

	print "Finished. Press any key to exit."
	getch()
	sys.exit(1)
	


main()


