#!/usr/bin/env python

#UWAGA!
#ten program wymaga zmodyfikowanej wersji scapy- standardowa na linuxa zawiera buga
#http://comments.gmane.org/gmane.comp.security.scapy.general/4699
#w klasie RTP scapy trzeba zamienic nazwe pola opisujacego payload-type protokolu RTP
#z "payload" na "payload_type"
#na Windowsie nie zauwazono problemu
#####################
# cd /usr/local/WowzaStreamingEngine-4.1.0/bin/
# sudo ./startup.sh
#####################
# sudo iptables -A OUTPUT -p udp -o your_interface -j NFQUEUE
#####################
# cd ~/oins_proj/
# sudo python send.py 
####################

import logging
l=logging.getLogger("scapy.runtime")
l.setLevel(49)

import os,sys,nfqueue,socket, random, struct
from scapy.all import *
from scapy.layers.rtp import RTP
from scapy.layers.inet import UDP
from Crypto.Cipher import AES

src_sync = None
last_byte = 0
data_len = 0
finished = False 
data = None
first_rtp_time = None
last_sent = 0

#glebsza modyfikacja transmisji
min_lack_payload_size = 900
max_lack_payload_size = 1100	#ramy rozmiaru jakie moze miec payload pakietu, zeby wstrzyknac do niego dane - dostosowac w zaleznosci od strumienia
key = '0123456789abcdef' #tym kluczem szyfrujemy, ale to moze byc proste, bo to tylko, zeby nei bylo widac tekstu w transmisji.
lack_mid_time = 140 #ms - sa opozniane o milisekundy w przedziale o szerokosci 20 o srodku w tej wartosci

#paramatry, ktore mozna zmieniac przy kazdej transmisji - nie trzeba nic zmieniac w readerze, a odczyta
time_to_start = 5 #s - czas po ilu sekundach od rozpoczecia nadawania ma zaczac sie transmisja
lack_pause = 1 #s - czas co ile najczesciej moga zostac podmieniane pakiety na lack




def process(i, payload):
	global src_sync, last_sent, finished, first_rtp_time, min_lack_payload_size
	if not finished :
		data = payload.get_data()
		pkt = IP(data)
		modified = False
	
		
		if first_rtp_time == None or time.time() - first_rtp_time > time_to_start :	#jesli nie rozpoczela sie jeszce transmisja rtp lub minal zadany czas od startu transmisji rtp
			if src_sync == None or time.time() - last_sent > lack_pause : 	#jesli nie ustawiono docelowego sourcesynca lub minal czas pauzy miedzy pakietami
				if pkt.haslayer(Raw):	#jesli zawiera nieodkodowany payload
					try :
						pkt.getlayer(UDP).decode_payload_as(RTP)	# to na rtp
					except :
						payload.set_verdict(nfqueue.NF_ACCEPT)		#czasem za krotkie, zeby to tak odkodowac, wtedy dalej
						return
					if pkt.payload_type == 96 :				#robimy transport tylko w tym payload typie, w ktorym zazwyczaj idzie najwiecej danych, zeby nie wzbudzac podejrzen
						if first_rtp_time == None :
							print "RTP connection detected..."	
							first_rtp_time = time.time()		#ustawienei startu transmisji rtp
						if len(pkt[Raw].load)>min_lack_payload_size and first_rtp_time != None:		#czy rozmiar danych miesci sie w zdefiniowane ramy
							if src_sync == None :
								src_sync = pkt[RTP].sourcesync			#jesli nie ma jeszce tokena synchronizacji, to ustaw go na ten, bo dla niego zapewne pakiety beda ok
								print "Transmission synchronization source has been set..."
							if src_sync == pkt[RTP].sourcesync:	#jesli sourcesync taki jak transmisji to wysylamy													
								#pkt.show()
								last_sent = time.time()		#ustaw czas ostatnio wyslanego pakietu
								pkt[Raw].load = bytes(get_data())	#wstrzykneicie naszych danych
								del pkt[UDP].chksum
								del pkt[IP].chksum		#scapy sam obliczy checksumy
								modified = True			#flaga, ze zmodyfikowano
								#print "modified to: "
								#pkt.show()
								print "{} of {} bytes sent...".format(last_byte, data_len)
			
	
		if modified:
			lack_time= random.randint(lack_mid_time - 10, lack_mid_time + 10)	#opoznij randomowy czas w danym przdziale
			time.sleep(lack_mid_time/1000.0) # na sekundy
			print "delay: {} ".format(lack_time)
			payload.set_verdict_modified(nfqueue.NF_ACCEPT, str(pkt), len(pkt))	
		else:
			payload.set_verdict(nfqueue.NF_ACCEPT)
	
		if finished :
			print "File sent! Exit by pressing ctrl + c and flush nfqueue."


def read_file(name):
	global data_len
	fh = open(name, 'rb')
	ba = bytearray(fh.read())
	data_len = len(ba)
	return ba

def get_filename():
	global data, key
	fname = raw_input("Please enter filename to send: ")
	try:	
		encrypt_file(key, fname)
		data = read_file("{}.enc".format(fname))
	except:
		print "Can't open file. Try again"
		get_filename()
	

def get_data():	#wczytuje odpowiedni przedzial danych do wyslania
	global data, finished, last_byte, data_len, min_lack_payload_size, max_lack_payload_size
	packet_data_size = random.randint(min_lack_payload_size, max_lack_payload_size)
	packet_data = data[last_byte:last_byte+packet_data_size]
	last_byte = last_byte+packet_data_size;
	if last_byte >= data_len : 
		finished = True
	return packet_data

def encrypt_file(key, in_filename, out_filename=None, chunksize=64*1024):
	if not out_filename:
		out_filename = in_filename + '.enc'

	iv = ''.join(chr(random.randint(0, 0xFF)) for i in range(16))
	encryptor = AES.new(key, AES.MODE_CBC, iv)
	filesize = os.path.getsize(in_filename)

	with open(in_filename, 'rb') as infile:
		with open(out_filename, 'wb') as outfile:
			outfile.write(struct.pack('<Q', filesize))
			outfile.write(iv)

		    	while True:
		        	chunk = infile.read(chunksize)
		        	if len(chunk) == 0:
		            		break
		        	elif len(chunk) % 16 != 0:
		            		chunk += ' ' * (16 - len(chunk) % 16)

		        	outfile.write(encryptor.encrypt(chunk))
	
		
def main():
	print "projekt OINS sender by Mateusz Koslacz and Pawel Jagiello."
	print ""
	print "WARNING!!! Scapy has a bug that preclude proper execution of this program. Check header comment of this file to find out how to fix it."
	print ""
	print "After entering name of file to send, sending will start immediately after RTP streaming is detected."
	print "Remember to use sudo `iptables -A OUTPUT -p udp -o your_interface -j NFQUEUE` before starting and run this script with root privileges."

	try:
		get_filename()
	except KeyboardInterrupt:
		print ""
		print "Interrupted by user, exiting..."
		sys.exit(1)

	q = nfqueue.queue()
	q.open()
	q.bind(socket.AF_INET)
	q.set_callback(process)

	try:
		q.create_queue(0)
	except RuntimeError, e:
		print "Whoops, we have a problem. Have you created a queue using iptables? Error message is: {}".format(e.message)
		sys.exit(1)

	try:
		print "OK. Waiting for RTP stream..."
		q.try_run()
	except KeyboardInterrupt:
		print ""
		print "Exiting..."
		q.unbind(socket.AF_INET)
		q.close()
		sys.exit(1)
	


main()

